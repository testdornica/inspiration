<?php

namespace Joveini\Inspire\Http\Controllers;

use Joveini\Inspire\Inspire;

class InspireController
{
    public function getRandomInspire(){
        $quote= Inspire::justDoIt();
        return view('inspire::index', compact('quote'));
    } 
}