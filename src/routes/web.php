<?php

use Illuminate\Support\Facades\Route;
use Joveini\Inspire\Http\Controllers\InspireController;

Route::get('inspire', [InspireController::class,"getRandomInspire"]);